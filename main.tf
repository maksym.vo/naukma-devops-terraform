terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.16"
    }
  }
  
  required_version = ">= 1.2.0"
}

provider aws {
  region = "eu-north-1"
}

locals {
  key_pair = "Voloshko_KP"
  instance_type = "t3.micro"
}

resource "aws_instance" "web" {
  ami           = data.aws_ami.ubuntu.id
  instance_type = local.instance_type
  key_name      = local.key_pair
  vpc_security_group_ids = [data.aws_security_group.selected.id]
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_size = 8
  }

  tags = {
    Name = "voloshko-lab5-terraform"
  }
}